/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.2.x                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       polyBoundaryMesh;
    location    "constant/polyMesh";
    object      boundary;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

7
(
    xMin
    {
        type            patch;
    }
    xMax
    {
        type            patch;
    }
    yMin
    {
        type            wall;
    }
    yMax
    {
        type            wall;
    }
    zMin
    {
        type            cyclic;
        matchTolerance  0.0001;
        neighbourPatch  zMax;
    }
    zMax
    {
        type            cyclic;
        matchTolerance  0.0001;
        neighbourPatch  zMin;
    }
    cylinder
    {
        type            wall;
    }
)

// ************************************************************************* //
